import React, {useEffect, useState} from 'react';
import instanciaAxios from './ajax/instanciaAxios.js'
import './PaginaTarefas.css'

const PaginaTarefas = () => {

    const [listaCategorias, setListaCategorias] = useState([]);
    const [listaTarefas, setListaTarefas] = useState([]);
    const [listaDias, setListaDias] = useState([]);
    const [listaTurnos, setListaTurnos] = useState([]);

    const [descricaoNovoItem, setDescricaoNovoItem] = useState("");
    const [categoriaNovoItem, setCategoriaNovoItem] = useState("");
    const [diaNovoItem, setDiaNovoItem] = useState("");
    const [turnoNovoItem, setTurnoNovoItem] = useState("");
    const [alertaNovoItem, setAlertaNovoItem] = useState("");

    const indiceUltimoElemento = listaTarefas.length - 1;
    const ultimoElemento = listaTarefas? listaTarefas[indiceUltimoElemento] : 0;
    const idUltimoelemento = ultimoElemento? ultimoElemento.id : 0;
    const idNovoItem = parseInt(idUltimoelemento) + 1;

    // const [showTabela, setShowTabela] = useState(true);

    /*Analogo ao form_load,
    faz ao entrar no componente depois de carregar o jsx.*/
    useEffect(() => {
        pegarCategorias();
        pegarTarefas();
        pegarDias();
        pegarTurnos();
    },[]);

    const pegarCategorias = async () => {
        try {
            const resposta = await instanciaAxios.get('../json/categorias.json');
            setListaCategorias(resposta.data.categorias);
        } catch(error) {
            console.log(error.message);
        };
    };

    const pegarTarefas = async () => {        
        try {
            const resposta = await instanciaAxios.get('../json/tarefas.json');
            setListaTarefas(resposta.data.tarefas);
        } catch(error) {
            console.log(error.message);
        };
    };

    const pegarDias = async () => {
        try {
            const resposta = await instanciaAxios.get('../json/dias.json');
            setListaDias(resposta.data.dias);
        } catch(error) {
            console.log(error.message);
        };
    }; 

    const pegarTurnos = async () => {
        try {            
            const resposta = await instanciaAxios.get('../json/turnos.json');
            setListaTurnos(resposta.data.turnos);
        } catch(error) {
            console.log(error.message);
        };
    };

    const OpcoesCategoriasComponentes = () => {
        const listaCategoriasJSX = listaCategorias.map((item) => {
            return <option key={item.id} value={item.id}>
                            {item.descricao}
                    </option>
        });
        return listaCategoriasJSX;
    };

    const CorpoTabelaComponentes = () => {
        return (
            <tbody>
                {listaTarefas.map((item) => {
                    // console.log("descricao:" + item.descricao);
                    // console.log("idCategoria:" + item.idCategoria);
                    // console.log("idDia:" + item.idDia);
                    // console.log("idTurno:" + item.idTurno);
                    // console.log("alerta:" + item.alerta);
                    return(
                        <LinhaTabelaComponente
                            key={item.id}    
                            tarefa={item}    
                            // id={item.id}                            
                            // descricao={item.descricao}
                            // categoria={item.idCategoria}
                            // dia={item.idDia}
                            // turno={item.idTurno}
                            // alerta={item.alerta}
                        />
                    );
                })}
            </tbody>
        );
    };

    const LinhaTabelaComponente = ({tarefa}) => {

        let categoriaTarefa = null;
        categoriaTarefa = listaDias ? 
            listaCategorias.find((item) => {
                return item.id === tarefa.idCategoria;                
            }) : null;

        let diaTarefa = null;
        diaTarefa = listaDias ? 
            listaDias.find((item) => {
                return item.id === tarefa.idDia;                
            }) : null;

        let turnoTarefa = null;
        turnoTarefa = listaTurnos ? 
            listaTurnos.find((item) => {
                return item.id === tarefa.idTurno;                
            }) : null;

        // let alertaTarefa = null;
        let imgAlerta = null;        
        let imgExcluir = null;
        if (tarefa.alerta === "ligado") {
            imgAlerta = <img alt="" className="image-alerta" src="/images/alarm.png" 
                            onClick={()=>{alterarAlarmeItem(tarefa.id)}} />;
        };

        imgExcluir = <img alt="" id="image-excluir" src="/images/deletar.bmp" 
                        onClick={()=>{removerItem(tarefa.id)}} />;    

        return (
            <tr key={tarefa.id}>
                <td> {imgAlerta}{tarefa? tarefa.descricao : null} </td>
                <td> {categoriaTarefa ? categoriaTarefa.descricao : null} </td>
                <td> {diaTarefa ? diaTarefa.rotulo : null} </td>
                <td> {turnoTarefa ? turnoTarefa.rotulo : null} </td>
                <td> {imgExcluir} </td>
            </tr>
        );
    }

    const RadioDiasComponentes = () => {
        if (listaDias.length > 0) {
            const listaDiasJSX = listaDias.map((item) => {            
                return  <div>
                            <input type="radio" 
                                name="dia"
                                id={`dia-${item.id}`} 
                                value={item.id}
                                key={`dia-${item.id}`}
                                checked={diaNovoItem === item.id} 
                                onChange={(event)=>{setDiaNovoItem(event.target.value)}} 
                            />
                            <label htmlFor={item.id}>{item.rotulo}</label>
                        </div>
            });
            return listaDiasJSX;
        } else {
            return null;
        }
    };

    const RadioTurnosComponentes = () => {
        if (listaTurnos.length > 0) {
            const listaTurnosJSX = listaTurnos.map((item) => {
                return  <div>
                            <input type="radio" 
                                name="turno" 
                                id={`turno-${item.id}`}
                                value={item.id}
                                key={`turno-${item.id}`}
                                checked={turnoNovoItem === item.id} 
                                onChange={(event)=>{setTurnoNovoItem(event.target.value)}} 
                            />
                            <label htmlFor={item.id}>{item.rotulo}</label>
                        </div>
            });
            return listaTurnosJSX;
        } else {
            return null;
        }
    };    

    const incluirItem = () => {
        if ((categoriaNovoItem && categoriaNovoItem !== -1) 
            && descricaoNovoItem 
            && diaNovoItem
            && turnoNovoItem
            ) {            
            const novoItem = {
                // "id" : "" + listaTarefas.length + "",
                "id" : "" + idNovoItem + "",
                "descricao" : descricaoNovoItem,
                "idCategoria" : categoriaNovoItem,
                "idDia" : diaNovoItem,
                "idTurno" : turnoNovoItem,
                "alerta" : alertaNovoItem
            };
            setListaTarefas([...listaTarefas, novoItem]);
            // console.log(novoItem);
        }else{
            if (!categoriaNovoItem) {
                alert("Escolha uma categoria.")
                return;
            }else{            
                if (!descricaoNovoItem) {
                    alert("Preencha a descrição.")
                    return;
                }else{
                    if (!diaNovoItem) {
                        alert("Escolha um dia.")
                        return;
                    }else{
                        if (!turnoNovoItem) {
                            alert("Escolha um turno.")
                            return;
                        };
                    };     
                };
            };
        };        
    };

    const removerItem = (id) => {
        // listaTarefas.
        if(id){
            const _listaTarefas = listaTarefas.filter((item)=>{
                return item.id !== id;
            });
            setListaTarefas(_listaTarefas);
        }
        // console.log("Apagando o item: " + id);
    }
    
    const alterarAlarmeItem = (id) => {
        const itemAltera = listaTarefas.find((item) => {
                    // return item.alarme = (item.alarme === 'ligado' ? 'desligado' : 'ligado');
                    return item.id === id;
                });
        itemAltera.alerta = 'desligado';

        const _listaTarefas = listaTarefas.filter((item)=>{
            return item.id !== id;
        });

        _listaTarefas.push(itemAltera);
        _listaTarefas.sort(function(item1, item2) { return item1.id - item2.id});
        
        setListaTarefas(_listaTarefas);

        //  alert("alterando o alarme do item: " + JSON.stringify(itemAltera));
    }
        
    return (
        <div>  
            <div className="tituloPagina"><header><h1>Cuidados com os Pets</h1></header></div>
            <div id="container">

                <div id="boxe-novo-item">                
                    <div id="lado-esquerdo">
                        <h2>Adicionar nova Tarefa</h2>
                        <div id="descricao">
                            <label htmlFor="campo-descricao">Descrição:</label>
                            <input type="text" name="campo-descricao" onChange={(event)=>{
                                    setDescricaoNovoItem(event.target.value)
                                }}
                            /> 
                        </div>

                        <div className="categoria">
                            <label>Categoria:</label>  
                            <select 
                                value ={categoriaNovoItem}
                                onChange={(event)=>{
                                setCategoriaNovoItem(event.target.value)
                                }}>
                                <option disabled defaultValue value="">Selecione uma categoria...</option>
                                <OpcoesCategoriasComponentes />
                            </select>                          
                        </div>

                        <div id="rbs-dia-turno">
                            <div id="dia">
                                <label>Dia</label>
                                <RadioDiasComponentes />
                            </div>
                            <div id="turno">
                                <label>Turno</label>
                                <RadioTurnosComponentes />
                            </div>     
                        </div>            

                        <div id="ligarAlerta">
                            <label htmlFor="alerta" >Alerta?</label>
                            <input type="checkbox" 
                                id="alerta" 
                                name="alerta"                             
                                checked={alertaNovoItem === 'ligado' ? true : false} 
                                    onChange={()=>{setAlertaNovoItem(alertaNovoItem === 'ligado' ? 'desligado' : 'ligado')}} 
                            />
                        </div>   

                        <div>
                            <button id="botao-enviar" onClick={(event) => {incluirItem(event)}}>Incluir</button>
                        </div>
                        
                    </div>
                </div>

                <div id="boxe-lista-tarefas">
                    <div id="lado-direito">                
                        <table>
                            <thead>
                                <tr>
                                    <th>Descrição</th>
                                    <th>Categoria</th>
                                    <th>Dia</th>
                                    <th>Turno</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            {/* {showTabela? (<CorpoTabelaComponentes />   ) : null} */}
                            <CorpoTabelaComponentes />
                            <tfoot>
                                <tr>
                                    <td colSpan='5'>Total de Itens: {listaTarefas.length}</td>
                                </tr>
                            </tfoot>
                                
                        </table>
                        {/* <button onClick={() =>setShowTabela(false)}>Excluir Tabela</button>    */}
                    </div>
                </div>
            </div>
            <div id="footer">
                <p>Cuidados com os Pets - Todos os direitos reservados</p>
            </div>
        </div>
    );
}

export default PaginaTarefas;